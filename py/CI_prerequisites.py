
import xarray as xr
import numpy as np

# COMPUTE THE MEAN TXnorm, THE 10TH PERCENTILE TXn10, AND THE 90TH PERCENTILE TGX90 IF REFERENCE PERIOD FILE INCLUDES
# DAILY MAX TEMPERATURES
ds = xr.open_dataset('/home/admin1/scratch/tasmax.nc', chunks=None)
ds.load()
ds = ds.drop_vars('time_bnds')
ds = ds.drop_vars('lat_bnds')
ds = ds.drop_vars('lon_bnds')
ds = ds.drop_vars('rotated_latitude_longitude')



ds_mean = ds.groupby('time.time').mean('time')
ds_perc10 = ds.reduce(np.percentile, dim='time', q=10)
ds_perc90 = ds.reduce(np.percentile, dim='time', q=90)
ds_mean.to_netcdf("/home/admin1/CI_prerequisites/TXnorm.nc")
ds_perc10.to_netcdf("/home/admin1/CI_prerequisites/TXn10.nc")
ds_perc90.to_netcdf("/home/admin1/CI_prerequisites/TXn90.nc")





# COMPUTE THE MEAN TNnorm, THE 10TH PERCENTILE TNn10, AND THE 90TH PERCENTILE TNn90 IF REFERENCE PERIOD FILE INCLUDES
# DAILY MIN TEMPERATURES
ds = xr.open_dataset('/home/admin1/scratch/tasmin.nc', chunks=None)
ds.load()
ds = ds.drop_vars('time_bnds')
ds_mean = ds.mean('time')
ds_perc10 = ds.reduce(np.percentile, dim='time', q=10)
ds_perc90 = ds.reduce(np.percentile, dim='time', q=90)
ds_mean.to_netcdf("/home/admin1/CI_prerequisites/TNnorm.nc")
ds_perc10.to_netcdf("/home/admin1/CI_prerequisites/TNn10.nc")
ds_perc90.to_netcdf("/home/admin1/CI_prerequisites/TNn90.nc")

# COMPUTE THE MEAN TGnorm, THE 10TH PERCENTILE TGn10, AND THE 90TH PERCENTILE TGn90 IF REFERENCE PERIOD FILE INCLUDES
# DAILY MEAN TEMPERATURES
ds = xr.open_dataset('/home/admin1/scratch/tas_.nc', chunks=None)
ds.load()
ds = ds.drop_vars('time_bnds')

ds_mean = ds.groupby('time.dayofyear').mean('time')

ds_roll_mean = ds_mean.rolling(dayofyear=5).mean()
ds_roll_mean = ds_roll_mean.rename({'dayofyear' : 'time'})
ds_roll_mean.to_netcdf("/home/admin1/CI_prerequisites/TGnorm2.nc")
ds_roll_mean = ds_roll_mean.drop_vars('lat_bnds')
ds_roll_mean = ds_roll_mean.drop_vars('lon_bnds')
ds_roll_mean = ds_roll_mean.drop_vars('rotated_latitude_longitude')
ds_roll_mean.to_netcdf("/home/admin1/CI_prerequisites/TGnorm2.nc")

ds_perc10 = ds.reduce(np.percentile, dim='time', q=10)
ds_perc90 = ds.reduce(np.percentile, dim='time', q=90)
ds_mean.to_netcdf("/home/admin1/CI_prerequisites/TGnorm.nc")
ds_perc10.to_netcdf("/home/admin1/CI_prerequisites/TGn10.nc")
ds_perc90.to_netcdf("/home/admin1/CI_prerequisites/TGn90.nc")