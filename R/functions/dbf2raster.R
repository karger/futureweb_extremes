#' Function to transform spp distribution database files into rasters
#' 
#' @param ssp distribtion database files
#' @returns raster.
#' @examples
#' load('./data/Network_properties.Rdata')
#' y <- sample(seq(1,100,1),20)
#' r <- dbf2raster(SPPPA = data.frame(PAGENAME = networkprops$Pagename, value = networkprops$VULSD), mask.dir = "Data/mask/")

dbf2raster <- function(SPPPA, mask.dir = NULL){
  # SPPPA must be in this format - first colmun with CELL ID and second column with the value (to plot)
  if(is.null(mask.dir)) stop("Must specify the mask file directory")
  library(raster)

  maskID <- read.dbf(list.files(path = mask.dir, full.names = TRUE, pattern = ".img.vat.dbf$"))
  maskk <- raster(x = list.files(path = mask.dir, full.names = TRUE, pattern = ".img$"))

  spp <- maskID
  spp$val <- NA
  spp$PageName <- as.character(spp$PageName)
  row.names(spp) <- spp$PageName

  SPPPA$PAGENAME <- as.character(SPPPA$PAGENAME)
  SPPPA[,2] <- as.numeric(as.character(SPPPA[,2]))
  row.names(SPPPA) <- SPPPA$PAGENAME

  cellID <- as.character(SPPPA$PAGENAME)
  if( nrow(spp[cellID,]) != nrow(SPPPA[cellID,])) stop("Cell IDs do not match")
  spp[cellID,"val"] <- SPPPA[cellID,2]

  xx <- raster::values(maskk)

  if( length(xx[!is.na(xx)]) != nrow(spp)) stop("Mask size inadequate")
  xx[!is.na(xx)] <- spp$val[xx[!is.na(xx)]]

  raster::values(maskk) <- xx
  return(maskk)
}

