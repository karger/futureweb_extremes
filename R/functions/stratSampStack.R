stratSampStack <- function(st1, size=100000, nstrata=20, 
                           nsamp=30, iter, ...) {
  df2 <- as.data.frame(st1)
  df2 <- df2[sample(nrow(df2), size, replace = T), ]
  df2 <- df2[complete.cases(df2), ]
  strata <- seq(min(df2$x), max(df2$x), max(df2$x)/nstrata)
  df2$stratum <- as.numeric(cut(df2$x, strata))
  dfn <- data.frame(x=numeric(), 
                    y=numeric(), 
                    stratum=numeric())
  for (n in 1:iter){
    sp <- split(df2, list(df2$stratum))
    samples <- lapply(sp, function(x) x[sample(1:nrow(x), nsamp, TRUE),])
    sdf2 <- do.call(rbind, samples)
    dfn <- rbind(dfn, sdf2)
    } 
  return(dfn)
  }

