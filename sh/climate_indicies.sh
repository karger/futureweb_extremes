#!/bin/bash

# CLIMATE INDICES WITH CDO
# THIS SCRIPT COMPUTES CLIMATE INDICES OF DAILY TEMPERATURE AND PRECIPITATION EXTREMES
# THE DEFINITION OF THESE CLIMATE INDICES ARE FROM THE EUROPEAN CLIMATE ASSESSMENT (ECA) PROJECT


# INPUT TO THIS BASH SCRIPT SHOULD BE DAILY DATA OF A CLIMATE VARIABLE AS A TIME SERIES IN NETCDF FORMAT
# PLEASE PROVIDE A PATH TO THE FILE AS THE FIRST ARGUMENT

path_to_file=$1
echo "Path to file: $path_to_file"

# IF A 2ND FILE IS GIVEN AS ARGUMENT IT SHOULD BE THE PATH TO IT OTHERWISE IT WILL BE -> ""
path_to_file_2=${2:-""}
echo "Path to 2nd input file: $path_to_file_2"

# DELETE THE PATH FROM THE NETCDF FILENAME
file="${path_to_file##*/}"
echo "Filename is: $file"

# DELETE THE FILE STRING FROM THE PATH
path="${path_to_file%/*}"
echo "Path: $path"

# DEFINE FOUR STRING VARIABLES WHICH HAS TO PART OF THE STRING TO PATH_TO_FILE
pr="pr"
tas="tas_"
tasmin="tasmin"
tasmax="tasmax"

# DELETE THE ENDING (DATA TYPE) FROM THE NETCDF FILENAME
filename="${file%.*}"
echo "Filename without ending: $filename"

# SET AN OUTPUT FOLDER
outputfolder=$3
echo "Output of script will be saved to: $outputfolder"

# COMPUTE THE MONTHLY MEAN OF THE TIME SERIES
echo "Computing monthly mean of the time series"
FILE=${outputfolder}/${filename}_mon_agg.nc
if [ -f "$FILE" ]; then
    echo "$FILE already exists"
else
    singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo monmean $path_to_file ${outputfolder}/${filename}_mon_agg.nc
fi

# COMPUTE THE YEARLY MEAN OF THE TIME SERIES
echo "Computing yearly mean of the time series"
FILE=${outputfolder}/${filename}_year_agg.nc
if [ -f "$FILE" ]; then
    echo "$FILE already exits"
else
    singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo yearmean $path_to_file ${outputfolder}/${filename}_year_agg.nc
fi

# 2.0.1 COMPUTE ECACDD (CONSECUTIVE DRY DAYS INDEX PER TIME PERIOD)
# IF INPUT FILE IS A TIME SERIES OF DAILY PRECIPITATION
if [[ "$filename" == *"$pr"* ]]; then
    echo "Computing consecutive dry days index for time series"
    FILE=${outputfolder}/${filename}_ecacdd.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exists"
    else
        singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_cdd $path_to_file ${outputfolder}/${filename}_ecacdd.nc
    fi
fi

# 2.0.2 COMPUTE ECACFD (CONSECUTIVE FROST DAYS INDEX PER TIME PERIOD)
# IF INPUT FILE IS A TIME SERIES OF DAILY MINIMUM TEMPERATURE
if [[ "$filename" == *"$tasmin"* ]]; then
    echo "Computing consecutive frost days index for time series"
    FILE=${outputfolder}/${filename}_ecacdf.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exits"
    else
        singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_cfd $path_to_file ${outputfolder}/${filename}_ecacfd.nc
    fi
fi

# 2.0.3 COMPUTE ECACSU (CONSECUTIVE SUMMER DAYS INDEX PER TIME PERIOD)
# IF INPUT FILE IS A TIME SERIES OF DAILY MAXIMUM TEMPERATURE
if [[ "$filename" == *"$tasmax"* ]]; then
    echo "Computing consecutive summer days index for time series"
    FILE=${outputfolder}/${filename}_ecacsu.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exits"
    else
        singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_csu $path_to_file ${outputfolder}/${filename}_ecacsu.nc
    fi
fi

# 2.0.4 COMPUTE ECACWD (CONSECUTIVE WET DAYS INDEX PER TIME PERIOD)
# IF INPUT FILE IS A TIME SERIES OF DAILY PRECIPITATION
if [[ "$filename" == *"$pr"* ]]; then
    echo "Computing consecutive wet days index for time series"
    FILE=${outputfolder}/${filename}_ecacwd.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exits"
    else
        singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_cwd $path_to_file ${outputfolder}/${filename}_ecacwd.nc
    fi
fi

# 2.0.5 COMPUTE ECACWDI (COLD WAVE DURATION INDEX W.R.T MEAN OF REFERENCE PERIOD)
if [[ "$filename" == *"$tasmin"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$TNnorm"* ]]
    then
        echo "No 2nd file provided to compute cold wave duration index (2.0.5)"
    else
        echo "Computing consecutive wet days index for time series"
        echo "2nd file should be the mean TNnorm of daily minimum temperature for any period as reference"
        singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_cwdi $path_to_file $path_to_file_2 ${outputfolder}/${filename}_ecacwdi.nc
    fi
fi

# 2.0.6 COMPUTE ECACWFI (COLD SPELL DAYS INDEX W.R.T 10TH PERCENTILE OF REFERENCE PERIOD)
if [[ "$filename" == *"$tas"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$TGn10"* ]]
    then
        echo "No 2nd file provided to compute cold wave duration index (2.0.6)"
    else
        echo "Computing cold spell days index for time series"
        echo "2nd file should be the 10th percentile TGn10 of daily mean temperature for any period as reference"
        singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_cwfi $path_to_file $path_to_file_2 ${outputfolder}/${filename}_ecacwfi.nc
    fi
fi

# 2.0.7 COMPUTE ECAETR (INTRA-PERIOD EXTREME TEMPERATURE RANGE)
# IF INPUT FILES ARE A TIME SERIES OF DAILY MAXIMUM AND MINIMUM TEMPERATURE
if [[ "$filename" == *"$tasmax"* ]]; then
    echo "Computing intra-period extreme temperature range for time series"
    FILE=${outputfolder}/${filename}_ecaetr.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exits"
    else
        path_to_file2=${path_to_file//$tasmax/$tasmin}
        echo $path_to_file2
        singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_etr $path_to_file $path_to_file2 ${outputfolder}/${filename}_ecaetr.nc
    fi
fi


# 2.0.8 COMPUTE ECAFD (FROST DAYS INDEX PER TIME PERIOD)
# IF INPUT FILE IS A TIME SERIES OF DAILY MINIMUM TEMPERATURE
if [[ "$filename" == *"$tasmin"* ]]; then
    echo "Computing frost days index for time series"
    FILE=${outputfolder}/${filename}_ecacdf.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exits"
    else
        singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_fd $path_to_file ${outputfolder}/${filename}_ecafd.nc
    fi
fi


# 2.0.10 COMPUTE ECAHD (HEATING DEGREE DAYS PER TIME PERIOD)
# IF INPUT FILE IS A TIME SERIES OF DAILY MEAN TEMPERATURE
if [[ "$filename" == *"$tas"* ]]; then
    echo "Computing heating degree days for time series"
    FILE=${outputfolder}/${filename}_ecahd.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exits"
    else
        singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_hd $path_to_file ${outputfolder}/${filename}_ecahd.nc
    fi
fi

# 2.0.11 COMPUTE ECAHWDI (HEAT WAVE DURATION INDEX W.R.T MEAN OF REFERENCE PERIOD)
if [[ "$filename" == *"$tasmax"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$TXnorm"* ]]
    then
        echo "No 2nd file provided to compute heat wave duration index (2.0.11)"
    else
        echo "Computing heat wave duration index for time series"
        echo "2nd file should be the mean TXnorm of daily maximum temperatures for any period as reference"
        for YEAR in 2061..2080
        do
          cdo -selyear,${YEAR} /mnt/bg1a/cordex/$RCP/$MODEL/tasmax_2061-2080.nc ~/scratch/temp_selyear.nc
          cdo eca_hwdi $path_to_file /home/admin1/CI_prerequisites/TXnorm2.nc ${outputfolder}/ecahwdi_${YEAR}.nc
        done
    fi
fi


# 2.0.13 COMPUTE ECAID (ICE DAYS INDEX PER TIME PERIOD)
# IF INPUT FILE IS A TIME SERIES OF DAILY MAXIMUM TEMPERATURE
if [[ "$filename" == *"$tasmax"* ]]; then
echo "Computing ice days index for time series"
    FILE=${outputfolder}/${filename}_ecacid.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exits"
    else
        singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_id $path_to_file ${outputfolder}/${filename}_ecaid.nc
    fi
fi

# 2.0.14 COMPUTE ECAR75P (MODERATE WET DAYS W.R.T 75TH PERCENTILE OF REFERENCE PERIOD)
if [[ "$filename" == *"$pr"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$RRn75"* ]]
    then
        echo "No 2nd file provided to compute moderate wet days index (2.0.14)"
    else
        echo "Computing moderate wet days index for time series"
        echo "2nd file should be the 75th percentile RRn75 of daily precipitation amount for any period as reference"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_r75p $path_to_file $path_to_file_2 ${outputfolder}/${filename}_eca_r75p.nc
    fi
fi

# 2.0.15 COMPUTE ECAR75PTOT (PRECIPITATION PERCENT DUE TO R75P DAYS)
if [[ "$filename" == *"$pr"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$RRn75"* ]]
    then
        echo "No 2nd file provided to compute precipitation percent due to R75p days index (2.0.15)"
    else
        echo "Computing precipitation percent due to R75p days index for time series"
        echo "2nd file should be the 75th percentile RRn75 of daily precipitation amount for any period as reference"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_r75ptot $path_to_file $path_to_file_2 ${outputfolder}/${filename}_eca_r75ptot.nc
    fi
fi

# 2.0.16 COMPUTE ECAR90P (WET DAYS W.R.T 90TH PERCENTILE OF REFERENCE PERIOD)
if [[ "$filename" == *"$pr"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$RRn90"* ]]
    then
        echo "No 2nd file provided to compute wet days index (2.0.16)"
    else
        echo "Computing wet days index for time series"
        echo "2nd file should be the 90th percentile RRn90 of daily precipitation amount for any period as reference"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_r90p $path_to_file $path_to_file_2 ${outputfolder}/${filename}_eca_r90p.nc
    fi
fi

# 2.0.17 COMPUTE ECAR90PTOT (PRECIPITATION PERCENT DUE TO R90P DAYS)
if [[ "$filename" == *"$pr"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$RRn90"* ]]
    then
        echo "No 2nd file provided to compute precipitation percent due to R90p days index (2.0.17)"
    else
        echo "Computing precipitation percent due to R90p days index for time series"
        echo "2nd file should be the 90th percentile RRn90 of daily precipitation amount for any period as reference"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_r90ptot $path_to_file $path_to_file_2 ${outputfolder}/${filename}_eca_r90ptot.nc
    fi
fi

# 2.0.18 COMPUTE ECAR95P (VERY WET DAYS W.R.T 95TH PERCENTILE OF REFERENCE PERIOD)
if [[ "$filename" == *"$pr"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$RRn95"* ]]
    then
        echo "No 2nd file provided to compute very wet days index (2.0.18)"
    else
        echo "Computing very wet days index for time series"
        echo "2nd file should be the 95th percentile RRn95 of daily precipitation amount for any period as reference"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_r95p $path_to_file $path_to_file_2 ${outputfolder}/${filename}_eca_r95p.nc
    fi
fi

# 2.0.19 COMPUTE ECAR95PTOT (PRECIPITATION PERCENT DUE TO R95P DAYS)
if [[ "$filename" == *"$pr"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$RRn95"* ]]
    then
        echo "No 2nd file provided to compute precipitation percent due to R95p days index (2.0.19)"
    else
        echo "Computing precipitation percent due to R95p days index for time series"
        echo "2nd file should be the 95th percentile RRn95 of daily precipitation amount for any period as reference"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_r95ptot $path_to_file $path_to_file_2 ${outputfolder}/${filename}_eca_r95ptot.nc
    fi
fi

# 2.0.20 COMPUTE ECAR99P (EXTREMELY WET DAYS W.R.T 99TH PERCENTILE OF REFERENCE PERIOD)
if [[ "$filename" == *"$pr"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$RRn99"* ]]
    then
        echo "No 2nd file provided to compute extremely wet days index (2.0.20)"
    else
        echo "Computing extremely wet days index for time series"
        echo "2nd file should be the 99th percentile RRn99 of daily precipitation amount for any period as reference"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_r99p $path_to_file $path_to_file_2 ${outputfolder}/${filename}_eca_r99p.nc
    fi
fi

# 2.0.21 COMPUTE ECAR99PTOT (PRECIPITATION PERCENT DUE TO R99P DAYS)
if [[ "$filename" == *"$pr"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$RRn99"* ]]
    then
        echo "No 2nd file provided to compute precipitation percent due to R99p days index (2.0.21)"
    else
        echo "Computing precipitation percent due to R99p days index for time series"
        echo "2nd file should be the 95th percentile RRn99 of daily precipitation amount for any period as reference"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_r99ptot $path_to_file $path_to_file_2 ${outputfolder}/${filename}_eca_r99ptot.nc
    fi
fi

# 2.0.22 COMPUTE ECAPD (PRECIPITATION DAYS INDEX PER TIME PERIOD)
# IF INPUT FILE IS A TIME SERIES OF DAILY PRECIPITATION
if [[ "$filename" == *"$pr"* ]]; then
    echo "Computing precipitation days index for time series"
    FILE=${outputfolder}/${filename}_ecar10mm.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exits"
    else
        echo "calc"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_r10mm $path_to_file ${outputfolder}/${filename}_ecar10mm.nc
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_r20mm $path_to_file ${outputfolder}/${filename}_ecar20mm.nc
    fi
fi

# 2.0.23 COMPUTE ECARR1 (WET DAYS INDEX PER TIME PERIOD)
# IF INPUT FILE IS A TIME SERIES OF DAILY PRECIPITATION
if [[ "$filename" == *"$pr"* ]]; then
    echo "Computing wet days index for time series"
    FILE=${outputfolder}/${filename}_ecarr1.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exits"
    else
              echo "calc"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_rr1 $path_to_file ${outputfolder}/${filename}_ecarr1.nc
    fi
fi

# 2.0.24 COMPUTE ECARX1DAY (HIGHEST ONE DAY PRECIPITATION AMOUNT PER TIME PERIOD)
# IF INPUT FILE IS A TIME SERIES OF DAILY PRECIPITATION
if [[ "$filename" == *"$pr"* ]]; then
    echo "Computing highest one day precipitation amount for time series"
    FILE=${outputfolder}/${filename}_ecarx1day.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exits"
    else
              echo "calc"
    #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_rx1day $path_to_file ${outputfolder}/${filename}_ecarx1day.nc
    fi
fi

# 2.0.25 COMPUTE ECARX5DAY (HIGHEST FIVE DAY PRECIPITATION AMOUNT PER TIME PERIOD)
# IF INPUT FILE IS A TIME SERIES OF DAILY PRECIPITATION
if [[ "$filename" == *"$pr"* ]]; then
    echo "Computing highest five day precipitation amount for time series"
    FILE=${outputfolder}/${filename}_ecarx5day.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exits"
    else
              echo "calc"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_rx5day $path_to_file ${outputfolder}/${filename}_ecarx5day.nc
    fi
fi

# 2.0.26 COMPUTE ECASDII (SIMPLE DAILY INTENSITY INDEX PER TIME PERIOD)
# IF INPUT FILE IS A TIME SERIES OF DAILY PRECIPITATION
if [[ "$filename" == *"$pr"* ]]; then
    echo "Computing the daily intensity index for time series"
    FILE=${outputfolder}/${filename}_ecasdii.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exits"
    else
              echo "calc"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_sdii $path_to_file ${outputfolder}/${filename}_ecasdii.nc
    fi
fi

# 2.0.27 COMPUTE ECASU (SUMMER DAYS INDEX PER TIME PERIOD)
# IF INPUT FILE IS A TIME SERIES OF DAILY MAXIMUM TEMPERATURE
if [[ "$filename" == *"$tasmax"* ]]; then
    echo "Computing number of summer days (T > 25�C) for time series"
    FILE=${outputfolder}/${filename}_ecacsu.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exits"
    else
              echo "calc"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_su $path_to_file ${outputfolder}/${filename}_ecasu.nc
    fi
fi

# 2.0.28 COMPUTE ECATG10P (COLD DAYS PERCENT W.R.T 10TH PERCENTILE OF REFERENCE PERIOD)
if [[ "$filename" == *"$tas"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$TGn10"* ]]
    then
        echo "No 2nd file provided to compute cold days percentage (2.0.28)"
    else
        echo "Computing cold days percentage for time series"
        echo "2nd file should be the 10th percentile TGn10 of daily mean temp. for any period as reference"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_tg10p $path_to_file $path_to_file_2 ${outputfolder}/${filename}_ecatg10p.nc
    fi
fi

# 2.0.29 COMPUTE ECATG90P (WARM DAYS PERCENT W.R.T 90TH PERCENTILE OF REFERENCE PERIOD)
if [[ "$filename" == *"$tas"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$TGn90"* ]]
    then
        echo "No 2nd file provided to compute warm days percentage (2.0.29)"
    else
        echo "Computing warm days percentage for time series"
        echo "2nd file should be the 90th percentile TGn90 of daily mean temp. for any period as reference"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_tg90p $path_to_file $path_to_file_2 ${outputfolder}/${filename}_ecatg90p.nc
    fi
fi

# 2.0.30 COMPUTE ECATN10P (COLD NIGHTS PERCENT W.R.T 10TH PERCENTILE OF REFERENCE PERIOD)
if [[ "$filename" == *"$tasmin"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$TNn10"* ]]
    then
        echo "No 2nd file provided to compute cold nights percentage (2.0.30)"
    else
        echo "Computing warm days percentage for time series"
        echo "2nd file should be the 10th percentile TNn10 of daily min. temp. for any period as reference"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_tn10p $path_to_file $path_to_file_2 ${outputfolder}/${filename}_ecatn10p.nc
    fi
fi

# 2.0.31 COMPUTE ECATN90P (WARM NIGHTS PERCENT W.R.T 90TH PERCENTILE OF REFERENCE PERIOD)
if [[ "$filename" == *"$tasmin"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$TNn90"* ]]
    then
        echo "No 2nd file provided to compute warm nights percentage (2.0.31)"
    else
        echo "Computing warm nights percentage for time series"
        echo "2nd file should be the 90th percentile TNn90 of daily min. temp. for any period as reference"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_tn90p $path_to_file $path_to_file_2 ${outputfolder}/${filename}_ecatn90p.nc
    fi
fi

# 2.0.32 COMPUTE ECATR (TROPICAL NIGHTS INDEX PER TIME PERIOD)
# IF INPUT FILE IS A TIME SERIES OF DAILY MINIMUM TEMPERATURE
if [[ "$filename" == *"$tasmin"* ]]; then
    echo "Computing tropical nights index for time series"
    FILE=${outputfolder}/${filename}_ecactr.nc
    if [ -f "$FILE" ]; then
        echo "$FILE already exits"
    else
    singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_tr $path_to_file ${outputfolder}/${filename}_ecatr.nc
fi
fi

# 2.0.33 COMPUTE ECATX10P (VERY COLD DAYS PERCENT W.R.T. 10TH PERCENTILE OF REFERENCE PERIOD)
# IF INPUT FILE IS A TIME SERIES DAILY MAXIMUM TEMPERATURE
if [[ "$filename" == *"$tasmax"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$TXn10"* ]]
    then
        echo "No 2nd file provided to compute very cold days percentage (2.0.33)"
    else
        echo "Computing cold days percentage"
        echo "2nd file should be 10th percentile TNn10 of daily max. temp. for any period as reference"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_tx10p $path_to_file ${outputfolder}/${filename}_eca_tx10p.nc
    fi
fi

# 2.0.34 COMPUTE ECATX90P (VERY WARM DAYS PERCENT W.R.T. 90TH PERCENTILE OF REFERENCE PERIOD)
# IF INPUT FILE IS A TIME SERIES DAILY MAXIMUM TEMPERATURE
if [[ "$filename" == *"$tasmax"* ]]; then
    if [ "$#" -eq "1" ] || [[ "$path_to_file_2" != *"$TXn90"* ]]
    then
        echo "No 2nd file provided to compute very warm days percentage (2.0.34)"
    else
        echo "Computing warm days percentage"
        echo "2nd file should be 90th percentile TXn90 of daily max. temp. for any period as reference"
        #singularity exec -B /storage /storage/karger/singularity/ubuntu_cdo_cdsapi_py.cont cdo eca_tx90p $path_to_file ${outputfolder}/${filename}_eca_tx90p.nc
    fi
fi
 