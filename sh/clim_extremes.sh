#!/bin/bash

# "cfd", "csu", "cwd", "ecacdd", "ecarr1", "ecasdii", "etr", "fd", "hd", "bio01d", "bio04d", "bio12d", "bio15d", "gdd5", "scd", "swe"
# create the directory structure
mkdir /mnt/bg1a/cordex/
for RCP in rcp45 rcp85
  do
  mkdir /mnt/bg1a//cordex/$RCP
    for MODEL in CSC-REMO2009 HIRHAM5 RACMO22E RCA4
    do
      mkdir /mnt/bg1a//cordex/$RCP/$MODEL/
      mkdir /mnt/bg1a/cordex/$RCP/$MODEL/eca
      mkdir /mnt/bg1a/cordex/$RCP/$MODEL/eca/ts
      mkdir /mnt/bg1a/cordex/$RCP/$MODEL/eca/nm
    done
  done

# get the temporal extent
for RCP in rcp45 rcp85
  do
    for MODEL in RCSC-REMO2009 HIRHAM5 RACMO22E RCA4
    do
      for VAR in tasmax tasmin tas pr
      do
        cdo selyear,2061/2080 /storage/karger/chelsa_V2/cordex/11km/$RCP/$MODEL/${VAR}BA.nc /mnt/bg1a/cordex/$RCP/$MODEL/${VAR}_2061-2080.nc
      done
    done
  done


for ECA in cfd csd cwd_ cwdi etr fd hd hwdi
do
  for RCP in rcp45 rcp85
  do
    for MODEL in CSC-REMO2009 HIRHAM5 RACMO22E RCA4
    do
      cdo mergetime /mnt/bg1a/cordex/$RCP/$MODEL/eca/ts/${ECA}* /mnt/bg1a/cordex/$RCP/$MODEL/eca/nm/${ECA}_2061-2080.nc
    done
  done
done


for ECA in cfd csd cwd_ cwdi etr fd hd hwdi
  do
    for RCP in obs
    do
      for MODEL in CHELSA
      do
        cdo mergetime /mnt/bg1a/cordex/$RCP/$MODEL/eca/ts/${ECA}* /mnt/bg1a/cordex/$RCP/$MODEL/eca/nm/${ECA}_1981-2005.nc
    done
  done
done


for ECA in cfd csd cwd_ cwdi etr fd hd hwdi
do
for RCP in rcp45 rcp85
  do
    for MODEL in HIRHAM5 RACMO22E RCA4
    do
      cdo mergetime /mnt/bg1a/cordex/$RCP/$MODEL/eca/ts/${ECA}* /mnt/bg1a/cordex/$RCP/$MODEL/eca/nm/${ECA}_2061-2080.nc
    done
  done
done