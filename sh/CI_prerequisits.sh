#!/bin/sh

# THE COMPUTATION OF CLIMATE INDICES WITH CDO USUALLY REQUIRES A 2ND INPUT FILE NEXT TO THE STANDARD INPUT FILE WHICH WILL ALWAYS BE A TIME SERIES OF DAILY PRECIPITATION (RR), MINIMUM TEMPERATURE (TN), MEAN TEMPERATURE (TG), MAXIMUM TEMPERATURE (TX)

# INPUT TO THIS BASH SCRIPT SHOULD BE A TIME SERIES OF DAILY PRECIPITATION (RR), MINIMUM TEMPERATURE (TN), MEAN TEMPERATURE (TG), OR MAXIMUM TEMPERATURE (TX) AS A PERIOD OF REFERENCE

# SET AN OUTPUT FOLDER
outputfolder='/home/admin1/CI_prerequisites'
echo "Output of this script will be saved to: $outputfolder"

cdo selyear,1982/2005 /storage/karger/chelsa_V2/cordex/11km/obs/tas_chunk.nc /storage/karger/chelsa_V2/cordex/11km/obs/tas_1981-2005.nc
cdo selyear,1982/2005 /storage/karger/chelsa_V2/cordex/11km/obs/tasmax_chunk.nc /storage/karger/chelsa_V2/cordex/11km/obs/tasmax_1982-2005.nc
cdo selyear,1982/2005 /storage/karger/chelsa_V2/cordex/11km/obs/tasmin_chunk.nc /storage/karger/chelsa_V2/cordex/11km/obs/tasmin_1982-2005.nc
cdo selyear,1982/2005 /storage/karger/chelsa_V2/cordex/11km/obs/pr.nc /storage/karger/chelsa_V2/cordex/11km/obs/pr_1982-2005.nc


cdo selyear,1981/2005 /storage/karger/chelsa_V2/cordex/11km/obs/tas_chunk.nc /storage/karger/chelsa_V2/cordex/11km/obs/tas_1981-2005.nc
cdo selyear,1981/2005 /storage/karger/chelsa_V2/cordex/11km/obs/tasmax_chunk.nc /storage/karger/chelsa_V2/cordex/11km/obs/tasmax_1981-2005.nc
cdo selyear,1981/2005 /storage/karger/chelsa_V2/cordex/11km/obs/tasmin_chunk.nc /storage/karger/chelsa_V2/cordex/11km/obs/tasmin_1981-2005.nc
cdo selyear,1981/2005 /storage/karger/chelsa_V2/cordex/11km/obs/pr.nc /storage/karger/chelsa_V2/cordex/11km/obs/pr_1981-2005.nc


cdo -setattribute,tas@units="degC" -addc,-273.15 -ydrunmean,5,rm=c /storage/karger/chelsa_V2/cordex/11km/obs/tas_1982-2005.nc /home/admin1/CI_prerequisites/TGnorm.nc
cdo -setattribute,tas@units="degC" -addc,-273.15 -ydrunmean,5,rm=c /storage/karger/chelsa_V2/cordex/11km/obs/tasmax_1982-2005.nc /home/admin1/CI_prerequisites/TXnorm.nc
cdo -setattribute,tas@units="degC" -addc,-273.15 -ydrunmean,5,rm=c /storage/karger/chelsa_V2/cordex/11km/obs/tasmin_1982-2005.nc /home/admin1/CI_prerequisites/TNnorm.nc


cdo ydrunmean,5,rm=c /storage/karger/chelsa_V2/cordex/11km/obs/tasmax_1982-2005.nc /home/admin1/CI_prerequisites/TXnorm_K.nc
cdo ydrunmean,5,rm=c /storage/karger/chelsa_V2/cordex/11km/obs/tasmin_1982-2005.nc /home/admin1/CI_prerequisites/Tnnorm_K.nc
cdo ydrunmean,5,rm=c /storage/karger/chelsa_V2/cordex/11km/obs/tas_1982-2005.nc /home/admin1/CI_prerequisites/TNnorm_K.nc


cdo ydrunmean,5,rm=c /storage/karger/chelsa_V2/cordex/11km/obs/tasmax_1981-2005.nc /home/admin1/CI_prerequisites/TXnorm_K_81.nc
cdo ydrunmean,5,rm=c /storage/karger/chelsa_V2/cordex/11km/obs/tasmin_1981-2005.nc /home/admin1/CI_prerequisites/TNnorm_K_81.nc
cdo ydrunmean,5,rm=c /storage/karger/chelsa_V2/cordex/11km/obs/tas_1981-2005.nc /home/admin1/CI_prerequisites/TGnorm_K_81.nc

