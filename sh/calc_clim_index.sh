
for MODEL in HIRHAM5 RCA4 RACMO22E #CSC-REMO2009
do
  for RCP in rcp45 rcp85
  do
        for YEAR in {2061..2080}
        do
          echo "Computing heat wave duration series"
          echo "2nd file should be the mean TXnorm of daily minimum temperature for any period as reference"
          cdo selyear,${YEAR} /mnt/bg1a/cordex/$RCP/$MODEL/tasmax_2061-2080.nc ~/scratch/tasmax_selyear.nc

          check=$(expr ${YEAR} % 4)
          if [ ${check} -eq 0 ]
          then
          echo 'leap year'
          cdo -delete,month=2,day=29 ~/scratch/tasmax_selyear.nc ~/scratch/tasmax_selyear_noleap.nc
          rm ~/scratch/tasmax_selyear.nc
          mv ~/scratch/tasmax_selyear_noleap.nc ~/scratch/tasmax_selyear.nc
          else
            echo 'no leap year'
          fi

          cdo eca_hwdi ~/scratch/tasmax_selyear.nc /home/admin1/CI_prerequisites/TXnorm_K.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/eca/ts/hwdi_${YEAR}.nc

          echo "Computing cold wave duration series"
          echo "2nd file should be the mean TNnorm of daily minimum temperature for any period as reference"
          cdo selyear,${YEAR} /mnt/bg1a/cordex/$RCP/$MODEL/tasmin_2061-2080.nc ~/scratch/tasmin_selyear.nc

          check=$(expr ${YEAR} % 4)
          if [ ${check} -eq 0 ]
          then
          echo 'leap year'
          cdo -delete,month=2,day=29 ~/scratch/tasmin_selyear.nc ~/scratch/tasmin_selyear_noleap.nc
          rm ~/scratch/tasmin_selyear.nc
          mv ~/scratch/tasmin_selyear_noleap.nc ~/scratch/tasmin_selyear.nc
          else
            echo 'no leap year'
          fi

          cdo eca_cwdi ~/scratch/tasmin_selyear.nc /home/admin1/CI_prerequisites/TNnorm_K.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/eca/ts/cwdi_${YEAR}.nc

          cdo eca_cfd ~/scratch/tasmin_selyear.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/eca/ts/cfd_${YEAR}.nc
          cdo eca_csu ~/scratch/tasmax_selyear.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/eca/ts/csd_${YEAR}.nc

          cdo selyear,${YEAR} /mnt/bg1a/cordex/$RCP/$MODEL/pr_2061-2080.nc ~/scratch/pr_selyear.nc
          cdo mulc,86400 ~/scratch/pr_selyear.nc ~/scratch/pr_selyear_mm.nc
          cdo eca_cwd ~/scratch/pr_selyear_mm.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/eca/ts/cwd_${YEAR}.nc

          cdo eca_etr ~/scratch/tasmax_selyear.nc ~/scratch/tasmin_selyear.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/eca/ts/etr_${YEAR}.nc

          cdo eca_fd ~/scratch/tasmin_selyear.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/eca/ts/fd_${YEAR}.nc

          cdo selyear,${YEAR} /mnt/bg1a/cordex/$RCP/$MODEL/tas_2061-2080.nc ~/scratch/tas_selyear.nc
          cdo eca_hd ~/scratch/tas_selyear.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/eca/ts/hd_${YEAR}.nc
        done
  done
done






for MODEL in CHELSA
do
  for RCP in obs
  do
        for YEAR in {1981..2005}
        do
          echo "Computing heat wave duration series"
          echo "2nd file should be the mean TXnorm of daily minimum temperature for any period as reference"
          cdo selyear,${YEAR} /mnt/bg1a/cordex/$RCP/$MODEL/tasmax_1981-2005.nc ~/scratch/tasmax_selyear.nc

          check=$(expr ${YEAR} % 4)
          if [ ${check} -eq 0 ]
          then
          echo 'leap year'
          cdo -delete,month=2,day=29 ~/scratch/tasmax_selyear.nc ~/scratch/tasmax_selyear_noleap.nc
          rm ~/scratch/tasmax_selyear.nc
          mv ~/scratch/tasmax_selyear_noleap.nc ~/scratch/tasmax_selyear.nc
          else
            echo 'no leap year'
          fi

          cdo eca_hwdi ~/scratch/tasmax_selyear.nc /home/admin1/CI_prerequisites/TXnorm_K.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/hwdi_${YEAR}.nc

          echo "Computing cold wave duration series"
          echo "2nd file should be the mean TNnorm of daily minimum temperature for any period as reference"
          cdo selyear,${YEAR} /mnt/bg1a/cordex/$RCP/$MODEL/tasmin_1981-2005.nc ~/scratch/tasmin_selyear.nc

          check=$(expr ${YEAR} % 4)
          if [ ${check} -eq 0 ]
          then
          echo 'leap year'
          cdo -delete,month=2,day=29 ~/scratch/tasmin_selyear.nc ~/scratch/tasmin_selyear_noleap.nc
          rm ~/scratch/tasmin_selyear.nc
          mv ~/scratch/tasmin_selyear_noleap.nc ~/scratch/tasmin_selyear.nc
          else
            echo 'no leap year'
          fi

          cdo eca_cwdi ~/scratch/tasmin_selyear.nc /home/admin1/CI_prerequisites/TNnorm_K.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/cwdi_${YEAR}.nc

          cdo eca_cfd ~/scratch/tasmin_selyear.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/cfd_${YEAR}.nc
          cdo eca_csu ~/scratch/tasmax_selyear.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/csd_${YEAR}.nc

          cdo selyear,${YEAR} /mnt/bg1a/cordex/$RCP/$MODEL/pr_1981-2005.nc ~/scratch/pr_selyear.nc
          cdo mulc,86400 ~/scratch/pr_selyear.nc ~/scratch/pr_selyear_mm.nc
          cdo eca_cwd ~/scratch/pr_selyear_mm.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/cwd_${YEAR}.nc

          cdo eca_etr ~/scratch/tasmax_selyear.nc ~/scratch/tasmin_selyear.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/etr_${YEAR}.nc

          cdo eca_fd ~/scratch/tasmin_selyear.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/fd_${YEAR}.nc

          cdo selyear,${YEAR} /mnt/bg1a/cordex/$RCP/$MODEL/tas_1981-2005.nc ~/scratch/tas_selyear.nc
          cdo eca_hd ~/scratch/tas_selyear.nc /mnt/bg1a/cordex/${RCP}/${MODEL}/hd_${YEAR}.nc
        done
  done
done












































